﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHT : MonoBehaviour
{
    public float velocidadX;
    public float maxX;
    private float posX;
    private float direction;

    public Transform ballPosition;

    // Update is called once per frame
    void Update()
    {
        if (ballPosition.position.y > 0)
        {
            if (ballPosition.position.x > transform.position.x)
            {
                direction = 1.0f;
            }
            else
            {
                direction = -1.0f;
            }
        }
        else
        {
            direction = 0;
        }

        posX = transform.position.x + direction*velocidadX*Time.deltaTime;

        if(posX>=maxX){
            posX = maxX;
        }else if(posX<=-maxX){
            posX = -maxX;
        }

        transform.position = new Vector3(posX, transform.position.y, transform.position.z);       
    }
}
